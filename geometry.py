import math
from typing import Union, Tuple, List

import cv2
from scipy.stats import linregress
import numpy as np


def create_image(w, h, dim):
    return np.zeros([h, w, dim] if dim > 1 else [h, w], dtype=np.uint8)


def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        return None

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y


class Point:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def tuple(self):
        return self.x, self.y

    def translated(self, x: int, y: int):
        return Point(self.x + x, self.y + y)

    def draw_circle(self, img, radius: int, color: List[int]):
        cv2.circle(img, self.tuple(), radius, color)

    def fill_circle(self, img, radius: int, color: List[int]):
        cv2.circle(img, self.tuple(), radius, color, cv2.FILLED)


class PointF:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def tuple(self):
        return self.x, self.y

    def tuple_int(self):
        return round(self.x), round(self.y)

    def rotated(self, angle: float):
        s = math.sin(angle)
        c = math.cos(angle)

        return PointF(self.x * c - self.y * s,
                      self.x * s - self.y * c)

    def rotated_deg(self, angle_deg: float):
        return self.rotated(angle_deg * math.pi / 180.0)

    def translated(self, x: float, y: float):
        return PointF(self.x + x, self.y + y)

    def dot(self, other: 'PointF'):
        return self.x * other.x + self.y * other.y

    def draw_circle(self, img, radius: float, color: Union[Tuple[int], Tuple[int, int, int]], thickness=None):
        if thickness is None:
            thickness = radius
        cv2.circle(img, self.tuple_int(), radius, color, thickness)

    def __add__(self, other: 'PointF'):
        return self.translated(other.x, other.y)

    def __mul__(self, other: float):
        return PointF(self.x * other, self.y * other)

    def __repr__(self):
        return f"PointF({self.x:.3f},{self.y:.3f})"


class LineF:
    def __init__(self, p1: PointF, p2: PointF):
        self.p1 = p1
        self.p2 = p2

    def get_intersection_point(self, other: 'LineF'):
        p = line_intersection(
                (
                    (self.p1.x, self.p1.y),
                    (self.p2.x, self.p2.y),
                ),
                (
                    (other.p1.x, other.p1.y),
                    (other.p2.x, other.p2.y),
                )
        )

        if p is None:
            return None

        return PointF(p[0], p[1])


class Rect:
    def __init__(self, x: int, y: int, w: int, h: int):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.center = Point(x + w // 2, y + h // 2)
        self.tl = Point(x, y)
        self.tr = Point(x + w, y)
        self.br = Point(x + w, y + h)
        self.bl = Point(x, y + h)

    def __repr__(self):
        return f"Rect({self.x},{self.y},{self.w},{self.h})"

    def crop_image(self, image):
        sx = self.x
        ex = self.x + self.w
        sy = self.y
        ey = self.y + self.h
        return image[sy:ey, sx:ex]

    def draw_rectangle(self, image, color, thickness):
        cv2.rectangle(image, self.tl.tuple(), self.br.tuple(), color, thickness)

    def fill_rectangle(self, image, color, thickness):
        cv2.fillPoly(image, pts=[np.array(self.get_poly())], color=color)

    def expanded(self, val: int) -> 'Rect':
        return Rect(self.x - val, self.y - val, self.w + val + val, self.h + val + val)

    def cropped_to(self, other: 'Rect'):
        s_left = self.tl.x
        s_right = self.tr.x
        s_top = self.tl.y
        s_bottom = self.br.y
        o_left = other.tl.x
        o_right = other.tr.x
        o_top = other.tl.y
        o_bottom = other.br.y
        return Rect.from_ltrb(
                max(s_left, o_left),
                max(s_top, o_top),
                min(s_right, o_right),
                min(s_bottom, o_bottom),
        )

    def get_poly(self):
        return [
            self.tl.tuple(),
            self.tr.tuple(),
            self.br.tuple(),
            self.bl.tuple(),
        ]

    @staticmethod
    def from_points(pt1: Union[Point, Tuple[int, int]], pt2: Union[Point, Tuple[int, int]]):
        pt1x = pt1.x if isinstance(pt1, Point) else pt1[0]
        pt1y = pt1.y if isinstance(pt1, Point) else pt1[1]
        pt2x = pt2.x if isinstance(pt2, Point) else pt2[0]
        pt2y = pt2.y if isinstance(pt2, Point) else pt2[1]
        return Rect.from_ltrb(pt1x, pt1y, pt2x, pt2y)

    @staticmethod
    def from_ltrb(left, top, right, bottom):
        return Rect(left, top, right - left, bottom - top)

    @staticmethod
    def from_xywh(x: int, y: int, w: int, h: int):
        return Rect(x, y, w, h)

    @staticmethod
    def from_image(image: np.ndarray):
        return Rect(0, 0, image.shape[1], image.shape[0])


import numpy as np


def fit_2d_line(points: List[PointF]):
    x_values = [v.x for v in points]
    y_values = [v.y for v in points]
    import scipy.stats
    xstd = np.std(x_values)
    ystd = np.std(y_values)
    # print(xstd, ystd)

    if xstd > ystd:
        reg = linregress(x_values, y_values)
        p1 = PointF(0, reg.intercept)
        p2 = PointF(100, reg.intercept + 100 * reg.slope)
        # print("X", reg.slope, reg.intercept)
        # print("p1", p1)
        # print("p2", p2)
    else:
        reg = linregress(y_values, x_values)
        # print("Y", reg.slope, reg.intercept)
        p1 = PointF(reg.intercept, 0)
        p2 = PointF(reg.intercept + 100 * reg.slope, 100)
        # print("p1", p1)
        # print("p2", p2)

    return LineF(p1, p2)


def put_image_on(dst, src, pt: Point):
    src_w = src.shape[1]
    src_h = src.shape[0]
    x, y = pt.x, pt.y
    dst[y:y + src_h, x:x + src_w] = src


def bbox(img):
    a = np.where(img != 0)
    t, b, l, r = np.min(a[0]), np.max(a[0]), np.min(a[1]), np.max(a[1])
    return Rect.from_ltrb(l, t, r, b)


if __name__ == "__main__":
    # l1 = LineF(PointF(0, 0), PointF(0, 1))
    # l2 = LineF(PointF(-1, 0), PointF(1, 1))
    #
    # p = l1.get_intersection_point(l2)
    # print(p)
    fit_2d_line([
        PointF(0, 0),
        PointF(1, 0.2),
        PointF(2, 0.4),
    ])
    fit_2d_line([
        PointF(0, 0),
        PointF(0, 2),
        PointF(0, 6),
    ])
    fit_2d_line([
        PointF(0, 0),
        PointF(2, 0),
        PointF(4, 0),
    ])
